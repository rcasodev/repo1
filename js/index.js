function storeUserData() {
	console.log("storeUserData");
  
  //Acceso via DOM
  var name = document.getElementById("formNombre").value;  
  console.log("Name es: "+name);
  
  var email = document.getElementById("formEmail").value;  
  console.log("Email es: "+email);
  
  var userData = {
  	"name": name,
    "email": email
  };
  
  //Acceso via web API
  localStorage.setItem("userDataJSON", JSON.stringify(userData));
  //sessionStorage.setItem("name", name);
  
}

function getUserData() {
	console.log("getUserData");
  event.preventDefault();
  
  //Acceso via DOM
  var userData = JSON.parse(localStorage.getItem("userDataJSON"));
  
  document.getElementById("formNombre").value = userData.name;
  document.getElementById("formEmail").value = userData.email;
  //Acceso via web API
  //localStorage.setItem("name", name);
  //sessionStorage.setItem("name", name);
  
}